import React, { Component } from "react";
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import GreyRoundBox from "./Partials/GreyRoundBox";
import QuestionBox from "./Partials/QuestionBox";
import {Question} from "./Services/Question";

export default class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			questions: [],
			email: null
		};

		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(event) {
		this.setState({
			value: event.target.value
		});
	}

	componentDidMount() {
		var that = this;
		Question.all().then(questions => {
			that.setState({questions: questions});
		});
	}

	render () {
		return (
			<div>
				<div className="col-md-10 offset-md-1 spacebox">
					<h3>Discover Your perspective</h3>
					<p>Complete the 7 min test and get a detailed report of your lenses on the world.</p>
				</div>
				<div className="col-md-8 offset-md-2">
					{this.state.questions.map((question, index) => 
						<GreyRoundBox>
							<QuestionBox key={index} question={question} />
						</GreyRoundBox>
					)}
					<GreyRoundBox>
						<div className="text-center">
							<h3>Your Email</h3>
							<input type="email"
								className="form-control"
								placeholder="you@example.com"
								value={this.state.email}
								onChange={this.handleChange} />
						</div>
					</GreyRoundBox>
					<div className="text-center spacebox">
						<button type="button" class="btn btn-primary">Save & Continue</button>
					</div>
				</div>
			</div>
		);
	}
};