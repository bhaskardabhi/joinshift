import React, { Component } from "react";

export default class GreyRoundBox extends Component {
	render() {
		return (
			<div className="row grey-round-box">
				<div className="col-md-8 offset-md-2">
					{this.props.children}
				</div>
			</div>
		);
	}
}