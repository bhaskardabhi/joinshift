import React, { Component } from "react";
import RoundCheckbox from "./RoundCheckbox";

export default class QuestionBox extends Component {
    render() {
        return (
            <div className="question-box text-center">
                <p className="title">{this.props.question.title}</p>
                <div className="checkboxes-container">
                    <span className="question-prefix">Disagree</span>
                    {Array.from({ length: 7 }, (item, index) => 
                        <RoundCheckbox key={index} value={item}/>
                    )}
                    <span className="question-suffix">Agree</span>
                </div>
            </div>
        );
    }
}