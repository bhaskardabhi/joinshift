import React, { Component } from "react";

export default class RoundCheckbox extends Component {
    render() {
        return (
            <div class="round-checkbox">
                <input type="checkbox" id={'checkbox-'+this.props.value} />
                <label for={'checkbox-'+this.props.value}></label>
            </div>
        );
    }
}