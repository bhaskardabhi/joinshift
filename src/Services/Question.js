import axios from 'axios';

export const Question = {
    all
};

function all(user) {
    return new Promise((resolve, reject) => {
        resolve([
            { title: "You consider yourself more practical than creative" },
            { title: "You often spend time exploring unrealistic and impractical yet intriguing ideas." },
            { title: "You consider yourself more practical than creative" },
            { title: "You often spend time exploring unrealistic and impractical yet intriguing ideas." },
            { title: "You consider yourself more practical than creative" },
            { title: "You often spend time exploring unrealistic and impractical yet intriguing ideas." },
            { title: "You consider yourself more practical than creative" },
            { title: "You often spend time exploring unrealistic and impractical yet intriguing ideas." },
        ]);
        return axios.get("api/v1/questions").then(response => {
            resolve([
                { title: "You consider yourself more practical than creative" },
                { title: "You often spend time exploring unrealistic and impractical yet intriguing ideas." },
                { title: "You consider yourself more practical than creative" },
                { title: "You often spend time exploring unrealistic and impractical yet intriguing ideas." },
                { title: "You consider yourself more practical than creative" },
                { title: "You often spend time exploring unrealistic and impractical yet intriguing ideas." },
                { title: "You consider yourself more practical than creative" },
                { title: "You often spend time exploring unrealistic and impractical yet intriguing ideas." },
            ]);
            resolve(response.data);
        }).catch(error => {
            reject(error);
        });
    });
}